"master" is the master branch having the master copy of the "project9-polls-and-rater".

From this a new development branch is created which contains the ongoing stable development code of project "project9-polls-and-rater".
From this development branch stable code will be merged in the master branch.

Below are the feature branches crreated from development branch in which individual developer will commit the development work and once some concrete development done they will raise a pull request to merge the code in development branch. Before sending pull request they must take a latest pull from development branch and resolve the conflicts if any.

Feature Branches named on developer name to easily identified:
feature/sanjay
feature/pragyan
feature/dimpal
