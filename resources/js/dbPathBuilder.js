DBPathBuilder = function () {
    const DB_BASE_PATH = 'https://pollrater.firebaseio.com';
    return {
        getDBPollPath : function () {
            return `${DB_BASE_PATH}/polls.json`;
        },
        getDBUserPath : function () {
            return `${DB_BASE_PATH}/users.json`;
        },
        getDBRaterPath : function () {
            return `${DB_BASE_PATH}/raters.json`;
        }
    }
}


