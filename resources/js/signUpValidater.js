
window.onload = function () {
    navigateUserToDashboard();
    document.getElementById("error").innerHTML = "";
    initializeFirebase();
    const signUpEmail = document.getElementById("signUpEmail");
    const signUpPassword = document.getElementById("signUpPassword1");
    const confirmPassword = document.getElementById("signUpPassword2");
    const signUpBtn = document.getElementById("signup");
    signUpBtn.addEventListener('click', e => {
        const email = signUpEmail.value;
        const password = signUpPassword.value;

        const fName = document.getElementById("firstname").value;
        const lName = document.getElementById("lastname").value;
        var pattern = /[a-zA-Z]+$/;
        if (!fName.match(pattern))
            document.getElementById("error").innerHTML = "First Name contains characters only";
        else if (!lName.match(pattern))
            document.getElementById("error").innerHTML = "Last Name contains characters only";
        else if (password != confirmPassword.value)
            document.getElementById("error").innerHTML = "Password and confirm password are not matched";
        else {
            const auth = firebase.auth();
            let user =
            {
                userName: email,
                fullName: fName + " " + lName
            };

            auth.createUserWithEmailAndPassword(email, password)
                .then(() => {
                    document.getElementById("error").innerHTML = "";
                    return saveUser(user);
                }).then((userId) => {
                    window.location.href = "login.html";
                })
                .catch(function (error) {
                    document.getElementById("error").innerHTML = error.message;
                });
        }
    });
}


