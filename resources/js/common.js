let saveLoggedInUserInfo = function (userId, fullName) {
    let user = {
        userId: userId,
        fullName: fullName
    }
    localStorage.setItem("LOGGEDIN_USER", JSON.stringify(user));
}

let getLoggedInUser = function () {
    let loggedInUser = localStorage.getItem("LOGGEDIN_USER");
    if (loggedInUser != null && loggedInUser != "") {
        return JSON.parse(loggedInUser);
    }
}

let getLoggedInUserID = function () {

    let user = getLoggedInUser();
    if (user != null) {
        return user.userId;
    } else {
        return null;
    }
}

let getLoggedInUserFullName = function () {
    let userData = getLoggedInUser();
    if (userData != null && userData != "") {
        if (userData.userId != null && userData.userId != "") {
            let user = getUserDataFromDB(userData.userId);
            if (user != null && user != "") {
                return user.fullName;
            } else {
                return null;
            }
        }
        return null;
    }
}

function showHideUsernameOnHeader() {
    let loggedInUser = getLoggedInUserFullName();
    if (loggedInUser === null || typeof loggedInUser === 'undefined') {
        if (document.forms[0] != null && document.forms[0].id === 'dashboardForm') {
            window.location.href = "./login.html";
        } else {
            $("#navDropDownLink").hide();
            $('#login').show();
            $('#dashboard').hide();
        }
    } else {
        $('#login').hide();
        $('#dashboard').show();
        $('#displayLoggedInUserFullName').html(loggedInUser);
    }
}

function navigateUserToDashboard() {
    let loggedInUser = getLoggedInUserFullName();
    if (typeof loggedInUser === 'undefined') {
        if (document.forms[0] != null && document.forms[0].id != 'loginForm'
            && document.forms[0].id != 'signUpForm') {
            window.location.href = "./login.html";
        } else {
            return;
        }
    } else {
        let actionType = localStorage.getItem("actionType");
        let userId = getLoggedInUserID();

        if (actionType != "" && (actionType === 'POLL' || actionType === 'RATER')) {
            document.getElementById("error").innerHTML = "";
            if (actionType != "" && actionType === 'POLL') {
                return savePollDataToDB(localStorage.getItem("NEW_POLL_DATA")).then((itemID) => {
                    localStorage.setItem("actionType", "");
                    return updateUserWithPollId(userId, itemID);
                }).then((result) => {
                    // saveLoggedInUserInfo(userId, result);
                    window.location.href = "./dashboard.html";
                });

            } else if (actionType != "" && actionType === 'RATER') {
                return saveRaterDataToDB(localStorage.getItem("NEW_RATER_DATA")).then((itemID) => {
                    localStorage.setItem("actionType", "");
                    return updateUserWithRaterId(userId, itemID);
                }).then((result) => {
                    // saveLoggedInUserInfo(userId, result);
                    window.location.href = "./dashboard.html";
                });
            }
        } else {
            window.location.href = "./dashboard.html";
        }
    }
}


function logout(afterLogoutNavURL) {
    localStorage.setItem("LOGGEDIN_USER", "");
    window.location.href = afterLogoutNavURL;
}
