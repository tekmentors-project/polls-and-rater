
initializeFirebase();

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var id = "imageof" + input.id;

    reader.onload = function (e) {
      let file = input.files[0];
      let storageRef = firebase.storage().ref('images/' + file.name);
      let task = storageRef.put(file);
      task.on('state_changed',
        function progress(snapshot) {
        },
        function error(err) {
          console.log("error")
        },
        function complete() {
          console.log("complete")
          storageRef.getDownloadURL()
            .then(function (url) {
              document.getElementById(id).setAttribute('src', url);
              console.log(url);
            })
            .catch(function (error) {
              console.log(error);
            })


        })
    }
  }
  reader.readAsDataURL(input.files[0]);
}

