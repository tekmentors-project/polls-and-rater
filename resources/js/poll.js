let raterDefaultTemplateName = "textImageBasedTemplate";

function validateForm() {
    let selectedPollOption = $("input[name='option_name']:checked").val();
    if (typeof selectedPollOption === 'undefined') {
        document.getElementById('errorText').innerHTML = "Please choose poll option.";
        return false;
    } else {
        document.getElementById('errorText').innerHTML = "";
        return true;
    }
}

//Funtion to handle the form submission
function onSubmit() {
    if (validateForm()) {
        let templateName = getTemplateNameFromLocalStorage();
        let pollOption = $("input[name='option_name']:checked").val();

        var data = getBasePollDataObject(templateName);
        var pollResult = data.result.options;

        for (var optionIndex in pollResult) {
            if (optionIndex === pollOption) {
                pollResult[optionIndex].count++;
                data.result.totalCount++;
            }
        }
        saveBasePollData(data, templateName);
        return true;
    } else {
        return false;
    }
}


//Function to be call on window load
window.onload = function () {
    let templateName = getTemplateNameFromLocalStorage();

    showHideUsernameOnHeader();

    let pollId=pollView();
    if (document.forms[0] != null && document.forms[0].id === 'basePollForm') {
        getBasePollDisplayData(templateName);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'basePollResultForm') {
        getBasePollResult(templateName);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'baseToCustomPollForm') {
        getBaseToCustomPollDisplayData(templateName);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'customTemplateForm') {
        getCustomPollDisplayData(pollId);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'customPollResult') {
        //console.log("id"+pollId)
        getCustomPollResult(pollId);
    }
    else if (document.getElementById("basePollRaterDataOnHomePage")) {
        getBasePollDataForHomePage(templateName);
    }
};

//Function to diplay the form data
var getBasePollResult = function (templateName) {

    var pollData = getBasePollDataObject(templateName);

    var basePollData = document.getElementById('basePollData');
    var basePollId = document.getElementById('base-template-id');

    document.getElementById('displayQuestionText').innerHTML = pollData.question.text;

    var options = pollData.question.options;
    var result = pollData.result;
    let totalCount = result.totalCount;
    var percentage = '';

    for (var optionIndex in options) {
        percentage = Math.round((result.options[optionIndex].count / totalCount) * 100);
        if (isNaN(percentage)) {
            percentage = "0%";
        } else {
            percentage = percentage + "%";
        }
        var basePollResultHtml = basePollId.innerHTML.replace('{{displayOptionID}}', optionIndex);
        basePollResultHtml = basePollResultHtml.replace('{{displayImage}}', options[optionIndex].image);
        basePollResultHtml = basePollResultHtml.replace('{{displayOptionText}}', options[optionIndex].text);
        basePollResultHtml = basePollResultHtml.replace('{{diplayProgressBarWidth}}', percentage);
        basePollResultHtml = basePollResultHtml.replace('{{displayOptionPollTotalCount}}', percentage);
        basePollResultHtml = basePollResultHtml.replace('{{displayOptionPollCount}}', result.options[optionIndex].count);

        var node = document.createElement('div');
        node.innerHTML = basePollResultHtml;
        basePollData.appendChild(node);
    }

    document.getElementById('displayTotalVotedPoll').innerHTML = totalCount;
}

//Function to diplay the form data
var getCustomPollResult = function (templateName) {
  //  console.log("temp"+pollData)
    var pollData = getCustomPollDataObject(templateName);
    //console.log("data"+pollData)
    var customPollData = document.getElementById('customPollData');
    var customPollId = document.getElementById('custom-poll-id');
    
    document.getElementById('displayQuestionText').innerHTML = pollData.question.text;

    var options = pollData.question.options;
    var result = pollData.result;
    let totalCount = result.totalCount;
    var percentage = '';

    for (var optionIndex in options) {
        percentage = Math.round((result.options[optionIndex].count / totalCount) * 100);
        if (isNaN(percentage)) {
            percentage = "0%";
        } else {
            percentage = percentage + "%";
        }
        var customPollHtml = customPollId.innerHTML.replace('{{displayOptionID}}', optionIndex);
        customPollHtml = customPollHtml.replace('{{displayImage}}', options[optionIndex].image);
        customPollHtml = customPollHtml.replace('{{displayOptionText}}', options[optionIndex].text);
        customPollHtml = customPollHtml.replace('{{diplayProgressBarWidth}}', percentage);
        customPollHtml = customPollHtml.replace('{{displayOptionPollTotalCount}}', percentage);
        customPollHtml = customPollHtml.replace('{{displayOptionPollCount}}', result.options[optionIndex].count);

        var node = document.createElement('div');
        node.innerHTML = customPollHtml;
        customPollData.appendChild(node);
    }

    document.getElementById('displayTotalVotedPoll').innerHTML = totalCount;
}

//Function to diplay the form data
var getBasePollDisplayData = function (templateName) {
    var data = getBasePollDataObject(templateName);
    storeToLocalStorage(data);
    console.log(data)
    var basePollData = document.getElementById('basePollData');
    var baseTemplateId = document.getElementById('base-template-id');

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options
    for (var optionIndex in options) {

        var basePollHtml = baseTemplateId.innerHTML.replace('{{displayOptionID}}', optionIndex);
        basePollHtml = basePollHtml.replace('{{displayOptionValue}}', optionIndex);
        basePollHtml = basePollHtml.replace('{{displayImage}}', options[optionIndex].image);
        basePollHtml = basePollHtml.replace('{{displayOptionRef}}', optionIndex);
        basePollHtml = basePollHtml.replace('{{displayOptionText}}', options[optionIndex].text);

        var node = document.createElement('div');
        node.innerHTML = basePollHtml;
        basePollData.appendChild(node);
    }
}

//Function to diplay the form data
var getBaseToCustomPollDisplayData = function (templateName) {
    var data = getBasePollDataObject(templateName);
    storeToLocalStorage(data);

    var customPollData = document.getElementById('customPollData');
    var customPollId = document.getElementById('custom-template-id');

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options;
    for (var optionIndex in options) {
        var customPollHtml = customPollId.innerHTML.replace('{{displayOptionID}}', optionIndex);
        customPollHtml = customPollHtml.replace('{{displayOptionValue}}', optionIndex);
        customPollHtml = customPollHtml.replace('{{displayOptionRef}}', optionIndex);
        customPollHtml = customPollHtml.replace('{{displayFileIndex}}', optionIndex);
        customPollHtml = customPollHtml.replace('{{displayImage}}', options[optionIndex].image);
        customPollHtml = customPollHtml.replace('{{displayImageId}}', "imageof" + optionIndex);
        customPollHtml = customPollHtml.replace('{{displayLabelId}}', "labelId" + optionIndex);

        //Don't show the image placeholder for text based template.
        if (templateName != "textBasedTemplate") {
            customPollHtml = customPollHtml.replace('{{imagePlacehoder}}', optionIndex);
            customPollHtml = customPollHtml.replace('{{divStyle}}', "display: block;");
        } else {
            customPollHtml = customPollHtml.replace('{{imagePlacehoder}}', optionIndex);
            customPollHtml = customPollHtml.replace('{{divStyle}}', "display: none;");
        }

        customPollHtml = customPollHtml.replace('{{displayOptionText}}', options[optionIndex].text);

        var node = document.createElement('div');
        node.innerHTML = customPollHtml;
        customPollData.appendChild(node);
    }

}


//Function to diplay the form data
var getCustomPollDisplayData = function (templateName) {
    console.log(templateName);
    var data = getCustomPollDataObject(templateName);
    storeToLocalStorage(data);
    setTemplateNameInLocalStorage(templateName);
    var customTemplateData = document.getElementById('customTemplateData');
    var customTemplateId = document.getElementById('custom-template-id');
    if (data != null) {
        document.getElementById('displayQuestionText').innerHTML = data.question.text;
        var options = data.question.options
        for (var optionIndex in options) {
            
            var customTemplateHtml = customTemplateId.innerHTML.replace('{{displayOptionID}}', optionIndex);
            customTemplateHtml = customTemplateHtml.replace('{{displayOptionValue}}', optionIndex);
            customTemplateHtml = customTemplateHtml.replace('{{displayImage}}', options[optionIndex].image);
            customTemplateHtml = customTemplateHtml.replace('{{displayOptionRef}}', optionIndex);
            customTemplateHtml = customTemplateHtml.replace('{{displayOptionText}}', options[optionIndex].text);
            customTemplateHtml = customTemplateHtml.replace('{{displayAction}}',"showCustomPollResult('"+templateName+"')");
            var node = document.createElement('div');
            node.innerHTML = customTemplateHtml;
            customTemplateData.appendChild(node);
        }
    }
}

//Function to diplay the form data
var getBasePollDataForHomePage = function () {
    var data = getBaseTemplateData();
    var content = "";
    let previewRedirectCall = '';
    let usePollRedirectCall = '';

    for (var item in data) {
        let path = data[item].templateImage;
        let title = data[item].question.text;
        let templateName = data[item].templateName;
        usePollRedirectCall = "redirectPageForUsePollAndUpdateLocalStorage('" + templateName + "')";
        previewRedirectCall = "redirectPageForPollPreviewAndUpdateLocalStorage('" + templateName + "')";
        content = content + '<div class="col-sm-6 col-lg-4 col-xl-3 mb-4"><div class="border">' +
            '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center text-light bg-secondary">POLL</h6></div>' +
            '<img class="card-img-top" width="50px" height="200px" src="' + path + '" alt="">' +
            '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center text-light bg-secondary">' + data[item].templateDescription + '</h6></div>' +
            '<div style="height:60px"><h6 class="card-title mt-2 mb-4 text-center">' + title + '</h6></div>' +
            '<div class="text-center"><button id="editBtn" class="btn btn-outline-success mr-2 mb-2" onclick=' + usePollRedirectCall + '>Use this Poll</button>' +
            '<button id="previewBtn" class="btn btn-outline-info ml-2 mb-2" onclick=' + previewRedirectCall + '>Preview Poll</button>' +
            '</div></div></div>';
    }

    var raterData = getBaseRaterDataObject(raterDefaultTemplateName);
    var options = raterData.question.options;
    let useRaterRedirectCall = "redirectPageForUseRaterAndUpdateLocalStorage(raterDefaultTemplateName)";
    let previewRaterRedirectCall = "redirectPageForRaterPreviewAndUpdateLocalStorage(raterDefaultTemplateName)";

    content = content + '<div class="col-sm-6 col-lg-4 col-xl-3 mb-4"><div class="border">' +
        '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center  text-light bg-success">RATER</h6></div>' +
        '<img class="card-img-top" width="50px" height="200px" src="' + raterData.templateImage + '" alt="">' +
        '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center  text-light bg-success">' + data[item].templateDescription + '</h6></div>' +
        '<div style="height:60px"><h6 class="card-title mt-2 mb-4 text-center">' + raterData.question.text + '</h6></div>' +
        '<div class="text-center"><button id="editBtn" class="btn btn-outline-success mr-2 mb-2" onclick=' + useRaterRedirectCall + '>Use this Rater</button>' +
        '<button id="previewBtn" class="btn btn-outline-info ml-2 mb-2" onclick=' + previewRaterRedirectCall + '>Preview Rater</button>' +
        '</div></div></div>';

    $(basePollRaterDataOnHomePage).html(content);
}




function redirectPageForUsePollAndUpdateLocalStorage(templateName) {
    setTemplateNameInLocalStorage(templateName);
    window.location.href = "./poll/createPoll.html";
}

function redirectPageForUseRaterAndUpdateLocalStorage(templateName) {
    setTemplateNameInLocalStorage(templateName);
    window.location.href = "./rater/createRater.html";
}

function redirectPageForPollPreviewAndUpdateLocalStorage(templateName) {
    setTemplateNameInLocalStorage(templateName);
    window.location.href = "./templates/basePoll.html";
}

function redirectPageForRaterPreviewAndUpdateLocalStorage(templateName) {
    setTemplateNameInLocalStorage(templateName);
    window.location.href = "./templates/baseRater.html";
}

function setTemplateNameInLocalStorage(templateName) {
    localStorage.setItem("templateName", templateName);
}

function getTemplateNameFromLocalStorage() {
    return localStorage.getItem("templateName");
}
function storeToLocalStorage(templateData) {
    localStorage.setItem("templateData", JSON.stringify(templateData));
}

function redirectToLoginPage() {
    window.location.href = "../login.html";
}

function updatePollData(templatedata, newPollData) {
    templatedata.question = newPollData;
    templatedata.result.totalCount = 0;
    $.each(templatedata.result.options, function (index, option) {
        console.log(option)
        option.count = 0;
    })
    return templatedata;
}

function createNewPollQuestion() {
    let pollQuestion = {};
    let pollOptions = {};
    $("input[name='option_name']").each(function () {
        let pollOption = {};
        let optionValue = this.value;
        var labelId = "#labelId" + optionValue;
        //pollOption.text = $(this).siblings().text();
        pollOption.text = $(labelId).text();
        var imageId = "#imageof" + optionValue;
        pollOption.image = $(imageId).attr("src");

        pollOptions[optionValue] = pollOption;
    });
    pollQuestion.text = $("#displayQuestionText").text();
    pollQuestion.image = "";
    pollQuestion.options = pollOptions;

    return pollQuestion;
}

function savePoll() {
    let templateData = JSON.parse(localStorage.getItem("templateData"));
    let newPollQuestion = createNewPollQuestion();
    let dataToSave = updatePollData(templateData, newPollQuestion);
    localStorage.setItem("NEW_POLL_DATA", JSON.stringify(dataToSave));
    localStorage.setItem("actionType", "POLL");
    redirectToLoginPage();
}

function showCustomPollResult(pollId) {
    
    let templateName = getTemplateNameFromLocalStorage();
    let pollOption = $("input[name='option_name']:checked").val();
    var data = getCustomPollDataObject(templateName);
    var pollResult = data.result.options;

    for (var optionIndex in pollResult) {
        if (optionIndex === pollOption) {
            pollResult[optionIndex].count++;
            data.result.totalCount++;
        }
    }
    saveCustomPollVote(data, templateName);
    window.location.href = "./pollResult.html?id="+pollId;
}
function pollView() {
    var urlParams = new URLSearchParams(window.location.search);
    title = window.location.href.slice(window.location.href.indexOf('?') + 1);
    var title = urlParams.get('title');
    var id = urlParams.get('id');
    //var data = getCustomPollData(id);
    return id;
}
