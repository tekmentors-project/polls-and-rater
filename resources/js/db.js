var DB_BASE_PATH = 'https://pollrater.firebaseio.com';

let dbPathBuilder = DBPathBuilder();

//Funtion to save the form data asynchronously using AJAX 
function saveBasePollData(formData, templateName) {
    $.ajax({
        url: DB_BASE_PATH + '/baseTemplates/' + templateName + '.json',
        type: 'PUT',
        data: JSON.stringify(formData),
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

//Funtion to save the form data asynchronously using AJAX 
function saveCustomPollVote(formData, templateName) {
    $.ajax({
        url: DB_BASE_PATH + '/polls/' + templateName + '.json',
        type: 'PUT',
        data: JSON.stringify(formData),
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

//Funtion to save the form data asynchronously using AJAX 
function saveCustomRaterVote(formData, templateName) {
    $.ajax({
        url: DB_BASE_PATH + '/raters/' + templateName + '.json',
        type: 'PUT',
        data: JSON.stringify(formData),
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


//Funtion to get the data from firbase asynchronously using AJAX 
function getBasePollDataObject(templateName) {
    var pollData;
    $.ajax({
        url: DB_BASE_PATH + '/baseTemplates/' + templateName + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            pollData = data;
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
    return pollData;
}


//Funtion to get the data from firbase asynchronously using AJAX 
function getCustomPollDataObject(templateName) {
    var pollData;
    $.ajax({
        url: DB_BASE_PATH + '/polls/' + templateName + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            pollData = data;
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
    return pollData;
}

//Funtion to get the data from firbase asynchronously using AJAX 
function getCustomRaterDataObject(templateName) {
    var raterData;
    $.ajax({
        url: DB_BASE_PATH + '/raters/' + templateName + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            raterData = data;
        },
        error: function (error) {
            console.log(error);
        }
    });
    return raterData;
}

//Funtion to get the data from firbase asynchronously using AJAX 
function getBaseTemplateData() {
    var result;
    $.ajax({
        url: DB_BASE_PATH + '/baseTemplates.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
            //console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });

    return result;
}


//Funtion to get the data from firbase synchronously using AJAX 
function getCustomPollRaterData(templateType, itemId) {
    var result;
    let dataUrl = templateType === 'POLL' ?
        DB_BASE_PATH + '/polls/' + itemId + '.json' : DB_BASE_PATH + '/raters/' + itemId + '.json'
    $.ajax({
        url: dataUrl,
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (error) {
            console.log(error);
        }
    });
    return result;
}

// function to save poll data after creating a poll

function savePollDataToDB(pollData) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: dbPathBuilder.getDBPollPath(),
            type: 'POST',
            data: pollData,
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                resolve(data.name);
            },
            error: function (error) {
                console.log(error);
                reject(error);
            }
        });
    })
}

// function to save poll data after creating a poll

function saveRaterDataToDB(raterData) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: dbPathBuilder.getDBRaterPath(),
            type: 'POST',
            data: raterData,
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                resolve(data.name);
            },
            error: function (error) {
                console.log(error);
                reject(error);
            }
        });
    })
}



//Funtion to get the data from firbase asynchronously using AJAX 
function getBaseRaterDataObject(templateName) {
    var raterData;
    $.ajax({
        url: DB_BASE_PATH + '/baseRaterTemplates/' + templateName + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            raterData = data;
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
    return raterData;
}

//Funtion to save the form data asynchronously using AJAX 
function saveBaseRaterData(formData, templateName) {
    $.ajax({
        url: DB_BASE_PATH + '/baseRaterTemplates/' + templateName + '.json',
        type: 'PUT',
        data: JSON.stringify(formData),
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

//Function to update data for loggedin user with the pollid created 

function updateUserWithPollId(userId, pollId) {
    let userData = getUserDataFromDB(userId);
    
    let pollIdList = [];
    if(typeof userData.createdPolls === 'undefined'){
        pollIdList.push(pollId);
        
    }else{
        pollIdList = userData.createdPolls;
        pollIdList.push(pollId);
    }
    userData.createdPolls = pollIdList;
    saveUserPollRaterData(userId, userData);

    return userData;
}

//Function to update data for loggedin user with the raterID created 

function updateUserWithRaterId(userId, raterId) {
    let userData = getUserDataFromDB(userId);
    
    let raterIdList = [];
    if(typeof userData.createdRaters === 'undefined'){
        raterIdList.push(raterId);
        
    }else{
        raterIdList = userData.createdRaters;
        raterIdList.push(raterId);
    }
    userData.createdRaters = raterIdList;
    saveUserPollRaterData(userId, userData);

    return userData;
}

function fetchDataNode(userRef, childName) {
    const query = userRef.child(childName);
    return query.once("value").then(function (snapshot) {
        return snapshot.val();
    });
}


// function to get created polls of loggedin user
function getUserCreatedPollRatersFromDB(templateType, userId) {
    var idlist;
    $.ajax({
        url: DB_BASE_PATH + '/users/' + userId + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            idList = data;
        },
        error: function (error) {
            console.log(error);
        }
    });
    return idList;
}


// function to save user after signup

function saveUser(Obj) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: dbPathBuilder.getDBUserPath(),
            type: 'POST',
            data: JSON.stringify(Obj),
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                resolve(data.name);
            },
            error: function (error) {
                console.log(error);
                reject(error);
            }
        });
    })
}

//Funtion to get the data from firbase asynchronously using AJAX 
function getUsersFromDB(userID) {
    var user;
    $.ajax({
        url: DB_BASE_PATH + '/users/' + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            user = data;
        },
        error: function (error) {
            console.log(error);
        }
    });
    return user;
}

//Funtion to get the data from firbase asynchronously using AJAX 
function getUserDataFromDB(userID) {
    var user;
    $.ajax({
        url: DB_BASE_PATH + '/users/' + userID + '.json',
        type: 'GET',
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            user = data;
        },
        error: function (error) {
            console.log(error);
        }
    });
    return user;
}

//Funtion to save the form data asynchronously using AJAX 
function saveUserPollRaterData(userID, userData) {
    $.ajax({
        url: DB_BASE_PATH + '/users/' + userID + '.json',
        type: 'PUT',
        data: JSON.stringify(userData),
        contentType: 'text/plain',
        dataType: 'json',
        async: false,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}