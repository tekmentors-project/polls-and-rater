
function getCustomPollDataForDashboardPage() {
    let pollIdList = getUserCreatedPollRaters("POLL");
    let createdPoll = pollIdList.createdPolls;
    let content = "";
    let pollShareLink = "";
    if (createdPoll != null) {
        for (var item in createdPoll) {
            let pollID = createdPoll[item];

            let pollData = getCustomPollRaterData("POLL", pollID);
            if (pollData != null) {
                let path = pollData.templateImage;
                let title = pollData.question.text;
                let socialTitle = title.replace(/\s/g, "_");

                content = content + '<div class="col-sm-6 col-lg-4 col-xl-3 mb-4"><div class="border">' +

                    '<a href="./poll/poll.html?id=' + pollID + '">' +
                    '<div><img class="card-img-top" width="50px" height="200px" src="' + path + '" alt=""></div></a>' +
                    '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center text-light bg-secondary">' + pollData.templateDescription + '</h6></div>' +
                    '<div style="height:60px"><h6 class="card-title mt-2 mb-4  text-center"><a class="text-dark" href="./poll/poll.html?id=' + pollID + '">' + title + '</a></h6></div>' +
                    '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center text-light bg-secondary"> <span class="mr-3">SHARE</span> ' +

                    '<span class="mr-4"><a href="http://www.facebook.com/sharer.php/?u=https://pollrater.firebaseapp.com/poll/poll.html?id=' + pollID + '"' +
                    ' title="Share on Facebook" target="_blank"><i class="fab fa-facebook-square fa-1x text-light"></i></a></span>' +
                    '<span class="mr-4"><a href="https://twitter.com/intent/tweet?source=webclient&amp;text=Try this poll, https://pollrater.firebaseapp.com/poll/poll.html?id=' + pollID + '"' +
                    ' title="Share on Twitter" target="_blank"><i class="fab fa-twitter fa-1x text-light font-weight-bold "></i></a></span>' +
                    '</h6></div></div></div></div></div>'
            }
        }
        $(displayPollDataOnDashboardPage).html(content);
    }
}

function getCustomRaterDataForDashboardPage() {
    let raterIdList = getUserCreatedPollRaters("RATER");
    let createdRaters = raterIdList.createdRaters;

    let content = "";
    let RaterShareLink = "";
    if (createdRaters != null) {
        for (var item in createdRaters) {
            let raterId = createdRaters[item];

            let raterData = getCustomPollRaterData("RATER", raterId);
            if (raterData != null) {
                let path = raterData.templateImage;
                let title = raterData.question.text;
                socialTitle = title.replace(/\s/g, "_");
                content = content + '<div class="col-sm-6 col-lg-4 col-xl-3 mb-4"><div class="border">' +

                    '<a href="./rater/rater.html?id=' + raterId + '">' +
                    '<div><img class="card-img-top" width="50px" height="200px" src="' + path + '" alt=""></div></a>' +
                    '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center text-light bg-secondary">' + raterData.templateDescription + '</h6></div>' +
                    '<div style="height:60px"><h6 class="card-title mt-2 mb-4 bg-light text-center"><a class="text-dark" href="./rater/rater.html?id=' + raterId + '">' + title + '</a></h6></div>' +
                    '<div style="height:30px"><h6 class="card-title pt-1 pb-2 text-center text-light bg-secondary"> <span class="mr-3">SHARE</span> ' +

                    '<span class="mr-4"><a href="http://www.facebook.com/sharer.php/?u=https://pollrater.firebaseapp.com/rater/rater.html?id=' + raterId + '"' +
                    ' title="Share on Facebook" target="_blank"><i class="fab fa-facebook-square fa-1x text-light"></i></a></span>' +
                    '<span class="mr-4"><a href="https://twitter.com/intent/tweet?source=webclient&amp;text=Try this rater, https://pollrater.firebaseapp.com/rater/rater.html?id=' + raterId + '"' +
                    ' title="Share on Twitter" target="_blank"><i class="fab fa-twitter fa-1x text-light font-weight-bold "></i></a></span>' +
                    '</h6></div></div></div></div></div>'

            }
        }

        $(displayRaterDataOnDashboardPage).html(content);
    }
}

window.onload = function () {
    showHideUsernameOnHeader();
    initializeFirebase();

    if (document.getElementById("displayPollDataOnDashboardPage")) {
        getCustomPollDataForDashboardPage();
    }
    if (document.getElementById("displayRaterDataOnDashboardPage")) {
        getCustomRaterDataForDashboardPage();
    }
}


function getUserCreatedPollRaters(type) {
    let userId = getLoggedInUserID();
    var idlist = getUserCreatedPollRatersFromDB(type, userId);
    return idlist;
}

function getUserCreatedRaters() {
    let userId = getLoggedInUserID();
    return getUserCreatedRatersFromDB(userId);
}
