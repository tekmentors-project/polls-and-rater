
window.onload = function () {

    navigateUserToDashboard();

    document.getElementById("error").innerHTML = "";
    initializeFirebase();
    const txtEmail = document.getElementById("email");
    const txtPassword = document.getElementById("password");
    const btnLogin = document.getElementById("login");
    btnLogin.addEventListener('click', e => {
        const email = txtEmail.value;
        const password = txtPassword.value;
        const auth = firebase.auth();
        let itemID = "";
        let userData = []
        auth.signInWithEmailAndPassword(email, password)
            .then(() => {
                userData = getUserIdForLoggedInUser(email);
                userID = userData[0];

                saveLoggedInUserInfo(userID, userData[1]);
                document.getElementById("error").innerHTML = "";

                let actionType = localStorage.getItem("actionType");
                if (actionType != "" && actionType === 'POLL') {
                    return savePollDataToDB(localStorage.getItem("NEW_POLL_DATA")).then((itemID) => {
                        localStorage.setItem("actionType", "");
                        return updateUserWithPollId(userID, itemID);
                    }).then((result) => {
                        window.location.href = "./dashboard.html";
                    });

                } else if (actionType != "" && actionType === 'RATER') {
                    return saveRaterDataToDB(localStorage.getItem("NEW_RATER_DATA")).then((itemID) => {
                        localStorage.setItem("actionType", "");
                        return updateUserWithRaterId(userID, itemID);
                    }).then((result) => {
                        window.location.href = "./dashboard.html";
                    });
                } else {
                    window.location.href = "./dashboard.html";
                }

            }).then((id) => {
                userID = id;
                let actionType = localStorage.getItem("actionType");
                if (actionType === 'POLL') {
                    localStorage.setItem("actionType", "");
                    return updateUserWithPollId(userID, itemID);
                } else if (actionType === 'RATER') {
                    localStorage.setItem("actionType", "");
                    return updateUserWithRaterId(userID, itemID);
                }
            }).then((result) => {
                window.location.href = "./dashboard.html";
            })
            .catch(error => {
                if (error.code === "auth/user-not-found") {
                    window.location.href = "./signUp.html";
                }
                else {
                    document.getElementById("error").innerHTML = error.message;
                }
            });
    });
}

function getUserIdForLoggedInUser(email) {
    let users = getUsersFromDB();
    var userArr = [];
    for (let userID in users) {
        let userData = getUserDataFromDB(userID);

        if (userData.userName === email) {
            userArr.push(userID);
            userArr.push(userData.fullName);
            break;
        }
    }
    return userArr;
}
