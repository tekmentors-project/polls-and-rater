function validateForm() {
    let selectedRaterOption1 = $("input[name='option1']:checked").val();
    let selectedRaterOption2 = $("input[name='option2']:checked").val();
    let selectedRaterOption3 = $("input[name='option3']:checked").val();

    if (typeof selectedRaterOption1 === 'undefined' | typeof selectedRaterOption2 === 'undefined' | typeof selectedRaterOption3 === 'undefined') {
        document.getElementById('errorText').innerHTML = "Please choose rater option in all the questions before vote.";
        return false;
    } else {
        document.getElementById('errorText').innerHTML = "";
        return true;
    }
}

//Funtion to handle the form submission
function onSubmit() {
    if (validateForm()) {
        let templateName = getTemplateNameFromLocalStorage();
        var data = getBaseRaterDataObject(templateName);
        var options = data.result.options;

        for (var optionIndex in options) {
            let raters = options[optionIndex].raters;
            let selectedOption = getSelectedRadioOption(optionIndex);
            console.log(selectedOption);
            for (var raterIndex in raters) {
                if (raterIndex === selectedOption) {
                    raters[raterIndex].count++;
                    data.result.options[optionIndex].totalCount++;
                }
            }
        }
        saveBaseRaterData(data, templateName);
        return true;
    } else {
        return false;
    }
}

function getSelectedRadioOption(radioName) {
    var radioGroup = document.getElementsByName(radioName);
    var selectedValue;
    for (var i = 0; i < radioGroup.length; i++) {
        if (radioGroup[i].checked) {
            selectedValue = radioGroup[i].value;
        }
    }
    return selectedValue;
}

//Function to be call on window load
window.onload = function () {
    let templateName = "textImageBasedTemplate";
    let raterId=raterView();
    showHideUsernameOnHeader();
    
    if (document.forms[0] != null && document.forms[0].id === 'baseRaterForm') {
        getBaseRaterDisplayData(templateName);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'baseRaterResultForm') {
        getBaseRaterResult(templateName);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'baseToCustomRaterForm') {
        getBaseToCustomRaterDisplayData(templateName);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'customTemplateForm') {
        getCustomRaterDisplayData(raterId);
    }
    else if (document.forms[0] != null && document.forms[0].id === 'customRaterResult') {
        getCustomRaterResult(raterId);
    }
    else if (document.getElementById("baseRaterDataOnHomePage")) {
        getBaseRaterDataForHomePage(templateName);
    }
    else if (document.getElementById("baseRaterDataOnDashboardPage")) {
        getCustomTemplateDataForDashboardPage();
    }
};

//Function to diplay the form data
var getBaseRaterResult = function (templateName) {
    var data = getBaseRaterDataObject(templateName);
    storeToLocalStorage(data);

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options;
    var content = '';
    let percentage = 0;

    for (var optionIndex in options) {
        content = content + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><div class="border"><img class="card-img-top" width="50px" height="200px" src="' + options[optionIndex].image + '" alt="">' +
            '<div class="card-body"><h6 class="card-title text-primary font-weight-bold">' + options[optionIndex].text + '</h6><hr>';

        var resultOptions = data.result.options[optionIndex];
        let totalCount = resultOptions.totalCount;
        let raters = resultOptions.raters;
        let counter = 0;
        for (var raterIndex in raters) {
            percentage = Math.round((raters[raterIndex].count / totalCount) * 100);
            if (isNaN(percentage)) {
                percentage = "0%";
            } else {
                percentage = percentage + "%";
            }
            counter++;
            content = content + '<h6 class="text-info text-left">Rating:' + counter + ' </h6><div class="progress"><div class="progress-bar progress-bar-striped bg-success" role="progressbar" ' +
                ' style="width:' + percentage + '" aria-valuemin="0" aria-valuemax="100">' +
                '<label class="form-check-label">' + percentage + '</label></div></div>';
                //' <strong class="pl-1 pr-1">[' + raters[raterIndex].count + ']</strong>
        }
        content = content + '</div></div></div>';
        // '<div class="mt-2"><strong>Total Vote: </strong>' + totalCount + '</div></div></div></div>';
    }
    $(baseRaterData).html(content);
}


//Function to diplay the form data
var getCustomRaterResult = function (templateName) {

    var data = getCustomRaterDataObject(templateName);
    storeToLocalStorage(data);

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options;
    var content = '';
    let percentage = 0;

    for (var optionIndex in options) {
        content = content + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><div class="border"><img class="card-img-top" width="50px" height="200px" src="' + options[optionIndex].image + '" alt="">' +
            '<div class="card-body"><h6 class="card-title text-primary font-weight-bold">' + options[optionIndex].text + '</h6><hr>';

        var resultOptions = data.result.options[optionIndex];
        let totalCount = resultOptions.totalCount;
        let raters = resultOptions.raters;
        let counter = 0;
        for (var raterIndex in raters) {
            percentage = Math.round((raters[raterIndex].count / totalCount) * 100);
            if (isNaN(percentage)) {
                percentage = "0%";
            } else {
                percentage = percentage + "%";
            }
            counter++;
            content = content + '<h6 class="text-info text-left">Rating:' + counter + ' </h6><div class="progress"><div class="progress-bar progress-bar-striped bg-success" role="progressbar" ' +
                ' style="width:' + percentage + '" aria-valuemin="0" aria-valuemax="100">' +
                '<label class="form-check-label">' + percentage + '</label></div></div>';
                //' <strong class="pl-1 pr-1">[' + raters[raterIndex].count + ']</strong>
        }
        content = content + '<div class="mt-2"><strong>Total Vote: </strong>' + totalCount + '</div></div></div></div>';
    }
    $(customRaterData).html(content);


}

//Function to diplay the form data
var getBaseRaterDisplayData = function (templateName) {
    var data = getBaseRaterDataObject(templateName);
    storeToLocalStorage(data);

    var baseRaterOptionsData = document.getElementById('baseRaterOptionsData');
    var baseTemplateOptionsId = document.getElementById('base-template-options-id');

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options;
    var content = '';
    for (var optionIndex in options) {
        content = content + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><div class="border"><img class="card-img-top" width="50px" height="200px" src="' + options[optionIndex].image + '" alt="">' +
            '<div class="card-body"><h6 class="card-title text-primary font-weight-bold">' + options[optionIndex].text + '</h6><hr>' +
            '<div><span class="pr-3 text-info">Low</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater1" value="rater1"><span class="pr-2">1</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater2" value="rater2"><span class="pr-2">2</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater3" value="rater3"><span class="pr-2">3 </span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater4" value="rater4"><span class="pr-2">4</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater5" value="rater5"><span class="pr-2">5</span>' +
            '<span class="pl-2 text-info">High</span></div></div></div></div>';
    }
    $(baseRaterData).html(content);
}

//Function to diplay the form data
var getBaseToCustomRaterDisplayData = function (templateName) {
    var data = getBaseRaterDataObject(templateName);
    storeToLocalStorage(data);

    var baseRaterOptionsData = document.getElementById('baseRaterOptionsData');
    var baseTemplateOptionsId = document.getElementById('base-template-options-id');

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options;
    var content = '';
    for (var optionIndex in options) {
        content = content + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><div class="border">' +
            '<img class="card-img-top" contenteditable="true" id="imageof' + optionIndex + '" width="50px" height="200px" src="' + options[optionIndex].image + '" alt="">' +
            '<input type="file" id="' + optionIndex + '" value="upload" onchange="readURL(this);"/>' +
            '<div class="card-body"><label class="card-title text-primary font-weight-bold" contenteditable="true" id="textof' + optionIndex + '">' + options[optionIndex].text + '</label><hr>' +
            '<div><span class="pr-3 text-info">Low</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater1" value="rater1"><span class="pr-2">1</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater2" value="rater2"><span class="pr-2">2</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater3" value="rater3"><span class="pr-2">3 </span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater4" value="rater4"><span class="pr-2">4</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater5" value="rater5"><span class="pr-2">5</span>' +
            '<span class="pl-2 text-info">High</span></div></div></div></div>';
    }
    $(baseRaterData).html(content);
}


//Function to diplay the form data
var getCustomRaterDisplayData = function (templateName) {

    var data = getCustomRaterDataObject(templateName);
    storeToLocalStorage(data);

    var baseRaterOptionsData = document.getElementById('baseRaterOptionsData');
    var baseTemplateOptionsId = document.getElementById('base-template-options-id');

    document.getElementById('displayQuestionText').innerHTML = data.question.text;

    var options = data.question.options;
    var content = '';
    for (var optionIndex in options) {
        content = content + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"><div class="border"><img class="card-img-top" contenteditable="true" width="50px" height="200px" src="' + options[optionIndex].image + '" alt="">' +
            '<div class="card-body"><h6 class="card-title text-primary font-weight-bold">' + options[optionIndex].text + '</h6><hr>' +
            '<div><span class="pr-3 text-info">Low</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater1" value="rater1"><span class="pr-2">1</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater2" value="rater2"><span class="pr-2">2</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater3" value="rater3"><span class="pr-2">3 </span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater4" value="rater4"><span class="pr-2">4</span>' +
            '<input type="radio" name="' + optionIndex + '" id="rater5" value="rater5"><span class="pr-2">5</span>' +
            '<span class="pl-2 text-info">High</span></div></div></div></div>';
    }
    $(baseRaterData).html(content);

}

//Function to diplay the form data
var getBaseRaterDataForHomePage = function () {
    var data = getBaseTemplateData();
    var content = "";
    let previewRedirectCall = '';
    let useRaterRedirectCall = '';
    for (var item in data) {
        let path = data[item].templateImage;
        let title = data[item].question.text;
        let templateName = data[item].templateName;
        useRaterRedirectCall = "redirectPageForUseRaterAndUpdateLocalStorage('" + templateName + "')";
        previewRedirectCall = "redirectPageForPreviewAndUpdateLocalStorage('" + templateName + "')";
        content = content + '<div class="col-4"><div class="border">' +
            '<img class="card-img-top img-fluid " src="' + path + '" alt="Card image" style=" width:100%">' +
            '<div><h4 class=" card-title"><b>' + title +
            '</b></h4></div>' +
            '<button id="editBtn" class="btn btn-success mb-1 mt-1 ml-1 mr-5" onclick=' + useRaterRedirectCall + '>Use this Rater</button>' +
            '<button id="previewBtn" class="btn btn-info mb-1 mt-1 ml-5" onclick=' + previewRedirectCall + '>Preview Rater</button>' +
            '</div></div>';
    }
    $(baseRaterDataOnHomePage).html(content);
}

//Function to diplay the form data
var getCustomTemplateDataForDashboardPage = function () {
    var data = getCustomRaterData();
    let keyIndex = 0;
    let key = '';
    let objectKeys = Object.keys(data);
    var content = "";
    let raterShareLink = "";

    for (var item in data) {
        key = objectKeys[keyIndex];
        keyIndex++;
        let path = data[item].templateImage;
        let title = data[item].question.text;
        content = content + '<div class="row"><div class="col-md-12">' +
            '<div class="card flex-md-row mb-4 box-shadow h-md-250">' +
            '<img class="card-img-right flex-auto d-none d-md-block" src="' + path + '" alt="Card image" style=" width:100%">' +
            '<div class="card-body d-flex flex-column align-items-start"><h6 class="mb-0">' +
            '<a class="text-dark" href="#">' + title +
            '</a></h6><br><div width="50px"> Share:&nbsp;&nbsp;' +
            '<a href="http://www.facebook.com/sharer.php/?u=http://www.proprofs.com/raters/rater/?id=' + key + '"' +
            ' title="Share on Facebook" target="_blank"><i class="fab fa-facebook-square fa-1x text-primary"></i></a>&nbsp;&nbsp;' +
            '<a href="https://twitter.com/intent/tweet?source=webclient&amp;text=Try this rater, http://www.proprofs.com/raters/rater/?id=' + key + '"' +
            ' title="Share on Twitter" target="_blank"><i class="fab fa-twitter fa-1x text-primary"></i></a>' +
            '</div></div></div></div> </div>';
    }

    $(baseRaterDataOnDashboardPage).html(content);
}


function redirectPageForUseRaterAndUpdateLocalStorage(templateName) {
    setTemplateNameInLocalStorage(templateName);
    window.location.href = "./rater/createRater.html";
}

function redirectPageForPreviewAndUpdateLocalStorage(templateName) {
    setTemplateNameInLocalStorage(templateName);
    window.location.href = "./templates/baseRater.html";
}

function setTemplateNameInLocalStorage(templateName) {
    localStorage.setItem("templateName", templateName);
}

function getTemplateNameFromLocalStorage() {
    return localStorage.getItem("templateName");
}
function storeToLocalStorage(templateData) {
    localStorage.setItem("templateData", JSON.stringify(templateData));
}

function redirectPage() {
    window.location.href = "../login.html";
}


function showCustomRaterResult() {
    let raterID = "-LLTQqTd7xw5qYFWJrG4";
    console.log(raterID);
    var data = getCustomRaterDataObject(raterID);
    console.log(data);
    var options = data.result.options;
    console.log(options);
    for (var optionIndex in options) {
        let raters = options[optionIndex].raters;
        let selectedOption = getSelectedRadioOption(optionIndex);
        console.log(selectedOption);
        for (var raterIndex in raters) {
            if (raterIndex === selectedOption) {
                raters[raterIndex].count++;
                data.result.options[optionIndex].totalCount++;
            }
        }
    }

    saveCustomRaterVote(data, raterID);
    window.location.href = "./raterResult.html";
}
function redirectToLoginPage() {
    window.location.href = "../login.html";
}

function saveRater() {
    let templateData = JSON.parse(localStorage.getItem("templateData"));
    var optionsParam = templateData.question.options;
    let newRaterQuestion = createNewRaterQuestion(optionsParam);
    let dataToSave = updateRaterData(templateData, newRaterQuestion);
    localStorage.setItem("NEW_RATER_DATA", JSON.stringify(dataToSave));
    localStorage.setItem("actionType", "RATER");
    redirectToLoginPage();
}

function createNewRaterQuestion(optionsParam) {
    var raterQuestion = {};
    var raterOptions = {};
    let options = optionsParam;

    debugger;
    for (var optionIndex in options) {
        var raterOption = {};
        var textid = "#textof" + optionIndex;
        raterOption.text = $(textid).html();
        var imageId = "#imageof" + optionIndex;
        raterOption.image = $(imageId).attr("src");
        raterOptions[optionIndex] = raterOption;
    }
    raterQuestion.text = $("#displayQuestionText").text();
    raterQuestion.image = "";
    raterQuestion.options = raterOptions;

    return raterQuestion;
}

function updateRaterData(templateData, newRaterData) {
    templateData.question = newRaterData;
    $.each(templateData.result.options, function (index, option) {
        $.each(option.raters, function (index, rater) {
            rater.count = 0;
        })
        option.totalCount = 0;
    })
    return templateData;
}
function raterView() {
    var urlParams = new URLSearchParams(window.location.search);
    title = window.location.href.slice(window.location.href.indexOf('?') + 1);
    var title = urlParams.get('title');
    var id = urlParams.get('id');
    //var data = getCustomPollData(id);
    return id;
}
